# Project Name: Raspibaby

## Description
Raspibaby is a project aimed at creating a smart baby monitoring system using Raspberry Pi. This project utilizes various sensors and modules to monitor the baby's environment and provide real-time data to parents or caregivers.

## Features
- Live video streaming
- Temperature and humidity monitoring
- Motion detection
- Sound detection
- 2-way Sound playback

## Hardware Requirements

### Main Components
- **Raspberry Pi** (Model 3B, 3B+, 4, or newer)
- **MicroSD Card** (16GB or larger, with Raspbian OS installed)
- **Power Supply** (5V 2.5A for Raspberry Pi 3 or 5V 3A for Raspberry Pi 4)
- **USB Microphone** (or compatible audio input device)
- **Speaker** (3.5mm jack or USB)

### Sensors and Modules
- **Camera Module** (Raspberry Pi Camera Module V2 or compatible)
- **MAX9814 Microphone Amplifier** (with Auto Gain Control)
- **MCP3008** (10-bit ADC - Analog to Digital Converter)
- **DHT22 Temperature and Humidity Sensor** (or DHT11 for lower accuracy)
- **PIR Motion Sensor** (HC-SR501 or compatible)

### Additional Components
- **Breadboard**
- **Jumper Wires** (Male-to-Male, Male-to-Female, Female-to-Female as needed)

### Connectivity
- **HDMI Cable** (for connecting Raspberry Pi to a monitor)
- **USB Keyboard and Mouse** (for initial setup and configuration)

### Optional
- **GPIO Extension Board** (for easier GPIO pin access and connection)
- **Heat Sinks** (for Raspberry Pi to manage heat better)
- **Enclosure/Case** (to house the Raspberry Pi and components for protection and aesthetics)



## Installation
1. Clone the repository: `git clone https://gitlab.com/yorel56/raspibaby.git`
2. Install [Icecast](https://icecast.org/)
3. Install [Liquadsoap](https://www.liquidsoap.info/)
4. Start Liquid script (optionally create a system service to start on boot)
5. Setup API project [API Readme](./api/README.md)
6. Setup Client project [Client Readme](./client/README.md)

## Usage
1. Connect the Raspberry Pi to the power source and ensure it is connected to the internet.
3. Monitor the baby's environment through the web client

## Contributing
Contributions are welcome! If you have any ideas or improvements, feel free to open an issue or submit a pull request.

## License
This project is licensed under the [MIT License](LICENSE).
