import React, {
  CSSProperties,
  SyntheticEvent,
  useCallback,
  useRef,
  useState,
} from "react";
import axios from "axios";
import { AudioRecorder } from "./AudioRecorder";
import { Form } from "react-bootstrap";

export const AudioPlayer = React.memo(() => {
    const handleFileDrop = useCallback((e: SyntheticEvent) => {
        if (e.target && (e.target as any).files) {
          const files = (e.target as any).files[0]
          let data = new FormData();
          data.append('audio', files);
  
          let config = {
              method: 'post',
              maxBodyLength: Infinity,
              url: 'http://<ENTER API ADDRESS>/play_audio',
              headers: {
                  "Content-Type": "multipart/form-data",
              },
              data : data
          };
  
          axios.request(config)
          .catch((error) => {
              console.log(error);
          });
        }
    }, []);
    const onRecordingFinished = useCallback((blob: Blob) => {
        let data = new FormData();
        data.append('audio', blob);

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'http://<ENTER API ADDRESS>/play_audio',
            headers: {
                "Content-Type": "multipart/form-data",
            },
            data : data
        };

        axios.request(config)
        .catch((error) => {
            console.log(error);
        });
    }, []);
  return (
    <>
      <div>
        <AudioRecorder onRecordingFinished={onRecordingFinished} />
      </div>
      <div id="playAudio">
        <strong>Select an audio file to play to your baby</strong>
        <Form onSubmit={(e) => e.preventDefault()}>
          <Form.Group>
            <Form.Control
              type="file"
              accept="audio/*"
              multiple={false}
              onChange={handleFileDrop}
            />
          </Form.Group>
        </Form>
      </div>
    </>
  );
});

AudioPlayer.displayName = "AudioPlayer";
