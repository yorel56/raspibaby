import React from "react";

export const VideoFeed = React.memo(() => {
    return(<>
        <img style={{
            width: "100%",
            height: "auto"
        
        }} src="http://<ENTER API ADDRESS>/video_feed"/>
        <audio>
            <source src="http://<ENTER API ADDRESS>/stream" type="application/ogg"/>
        </audio>
    </>)
})
VideoFeed.displayName = "VideoFeed";