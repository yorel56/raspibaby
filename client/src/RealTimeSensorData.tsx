import axios from "axios";
import React from "react";
import { Row } from "react-bootstrap";
import Switch from "react-switch";

export const RealTimeSensorData = React.memo(() => {
    const fetchSensorDataTimer = React.useRef<number | undefined>();
  const [inMetric, setInMetric] = React.useState(true);
  const [sensorData, setSensorData] = React.useState<{
    temperature: number;
    humidity: number;
    pressure: number;
    sound_level: number;
    motion_detected: boolean;
  } | undefined>();
  const fetchSensorData = React.useCallback(() => {
    axios.get("http://<ENTER API ADDRESS>/sensor_data").then((response) => {
        setSensorData(response.data);
    }).catch((error) => {
        console.error("Error fetching sensor data", error);
        window.clearInterval(fetchSensorDataTimer.current);
    });
  }, []);
  const convertToFahrenheit = React.useCallback((celsius: number) => {
    return celsius * 9 / 5 + 32;
  }, [])
    React.useEffect(() => {
        if(fetchSensorDataTimer.current) {
            window.clearInterval(fetchSensorDataTimer.current);
        }
        fetchSensorDataTimer.current = window.setInterval(fetchSensorData, 2000);
        return () => {
        if (fetchSensorDataTimer.current) {
            window.clearInterval(fetchSensorDataTimer.current);
        }
        };
    }, []);
  return (
    <>
      <Row>
        <h4>Real Time Sensor Data</h4>
        <p>This is the real time sensor data.</p>
      </Row>
      <Row>
        <div>
          <span id="useMetric">
            Use Metric Units ({inMetric ? "ON" : "OFF"})
          </span>
          <Switch checked={inMetric} onChange={setInMetric} />
        </div>
        <div>
            <span>Temperature: </span>
            <span>
                {inMetric ? sensorData?.temperature.toFixed(2) : convertToFahrenheit(sensorData?.temperature).toFixed(2)}
                {inMetric ? "°C" : "°F"}
            </span>
        </div>
        <div>
            <span>Humidity: </span>
            <span>{sensorData?.humidity.toFixed(2)}%</span>
        </div>
        <div>
            <span>Pressure: </span>
            <span>{sensorData?.pressure.toFixed(2)} hPa</span>
        </div>
        <div>
            <span>Sound Level: </span>
            <span>{sensorData?.sound_level.toFixed(2)} dB</span>
        </div>
        <div>
            <span>Motion Detected: </span>
            <span>{sensorData?.motion_detected ? "Yes" : "No"}</span>
        </div>
      </Row>
    </>
  );
});

RealTimeSensorData.displayName = "RealTimeSensorData";
