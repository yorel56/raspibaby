import React, { useState, useRef, useCallback } from "react";
import { Button } from "react-bootstrap";

interface Props {
    onRecordingFinished: (blob: Blob) => void;
}

export const AudioRecorder = React.memo((props: Props) => {
    const [isRecording, setIsRecording] = useState(false);
    const mediaStream = useRef(null);
    const mediaRecorder = useRef(null);
    const chunks = useRef([]);
    const startRecording = useCallback(async () => {
        setIsRecording(true);
      try {
        const stream = await navigator.mediaDevices.getUserMedia(
          { audio: true }
        );
        mediaStream.current = stream;
        mediaRecorder.current = new MediaRecorder(stream);
        mediaRecorder.current.ondataavailable = (e) => {
          if (e.data.size > 0) {
            chunks.current.push(e.data);
          }
        };
        mediaRecorder.current.onstop = () => {
          const recordedBlob = new Blob(
            chunks.current, { type: 'audio/webm' }
          );
          const url = URL.createObjectURL(recordedBlob);
          props.onRecordingFinished(recordedBlob);
          setIsRecording(false);
          chunks.current = [];
        };
        mediaRecorder.current.start();
      } catch (error) {
        setIsRecording(false);
        console.error('Error accessing microphone:', error);
      }
    }, [props.onRecordingFinished]);
    const stopRecording = useCallback(() => {
      if (mediaRecorder.current && mediaRecorder.current.state === 'recording') {
        mediaRecorder.current.stop();
      }
      if (mediaStream.current) {
        mediaStream.current.getTracks().forEach((track) => {
          track.stop();
        });
      }
    }, []);
    return (
      <div style={{marginTop: 16}}>
        <strong>You can record audio to send to you baby by clicking the buttons below</strong>
        <br/>
        <br/>
        <Button disabled={isRecording} variant={isRecording ? "outline-success" : undefined} onClick={startRecording}>{isRecording ? "Recording..." : "Start Recording"}</Button>
        <Button disabled={!isRecording} style={{marginLeft: 32}} variant="outline-danger" onClick={stopRecording}>Stop Recording</Button>
      </div>
    );
  });