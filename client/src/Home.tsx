import React from 'react';
import { Col, Container, Navbar, Row } from 'react-bootstrap';
import { VideoFeed } from './VideoFeed';
import { RealTimeSensorData } from './RealTimeSensorData';
import { HistoricalDataCharts } from './HistoricalDataCharts';
import { AudioPlayer } from './AudioPlayer';

class Home extends React.Component<{}, {}> {
  public render() {
    return (
      <Container className="Home">
        <Navbar className="bg-body-tertiary">
          <Container>
            <Navbar.Brand href="#home">Raspi-Baby</Navbar.Brand>
          </Container>
        </Navbar>
        <div className='title'>
          <h1>Welcome to Raspi-Baby</h1>
          <p>
            This is a simple web application that demonstrates how to use a Raspberry Pi to monitor a baby (hopefully your own baby).
          </p>
        </div>
        <Row>
          <Col sm={{span: 12}} md={{span: 8}}>
            <VideoFeed />
          </Col>
          <Col sm={{span: 12}} md={{span: 4}}>
            <Row>
              <Col>
                <RealTimeSensorData />
              </Col>
            </Row>
            <Row>
              <Col>
                  <AudioPlayer />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <HistoricalDataCharts />
        </Row>
        <footer>
          Designed and developed by <a href="https://www.codinghouse.dev">Coding House (TreJon House)</a>
        </footer>
      </Container>
    );
  }
}

export default Home;