import React from 'react';
import axios from 'axios';
import { DateTime } from 'luxon';
import Switch from "react-switch";

import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const SensorChart = ({ sensorData, dataKey }) => {
    return (
      <LineChart
        width={800}
        height={400}
        data={sensorData}
        margin={{ top: 20, right: 30, left: 20, bottom: 10 }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="readableTime" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey={dataKey} stroke="#8884d8" />
      </LineChart>
    );
  };

export const HistoricalDataCharts = React.memo(() => {

    const fetchSensorDataTimer = React.useRef<number | undefined>();
  const [inMetric, setInMetric] = React.useState(true);
  const [sensorData, setSensorData] = React.useState<{
    temperature: number;
    humidity: number;
    pressure: number;
    sound_level: number;
    motion_detected: boolean;
    timestamp: number;
    readableTime: string;
    id: number;
    isMetric: boolean;
  }[]>([]);
  const convertToFahrenheit = React.useCallback((celsius: number) => {
    return celsius * 9 / 5 + 32;
  }, [])
  const convertToCelsius = React.useCallback((fahrenheit: number) => {
    return (fahrenheit - 32) * 5 / 9;
  }, [])
  const fetchSensorData = React.useCallback(() => {
    axios.get("http://<ENTER API ADDRESS>/historical_data").then((response) => {
        setSensorData(response.data.map((data) => ({
            ...data,
            isMetric: inMetric,
            temperature: inMetric ? data.temperature : convertToFahrenheit(data.temperature),
            readableTime: DateTime.fromMillis(data.timestamp).toFormat("hh:mm:ss a")
        })));
    }).catch((error) => {
        console.error("Error fetching sensor data", error);
        window.clearInterval(fetchSensorDataTimer.current);
    });
  }, [inMetric]);

  React.useEffect(() => {
    setSensorData((prevData) => prevData.map((data) => (
     {
        ...data,
        temperature: inMetric ? data.isMetric ? data.temperature : convertToCelsius(data.temperature) : !data.isMetric ? data.temperature : convertToFahrenheit(data.temperature),
        isMetric: inMetric
     }   
    )));
  }, [inMetric])
  
    React.useEffect(() => {
        if(fetchSensorDataTimer.current) {
            window.clearInterval(fetchSensorDataTimer.current);
        }
        fetchSensorDataTimer.current = window.setInterval(fetchSensorData, 2000);
        return () => {
        if (fetchSensorDataTimer.current) {
            window.clearInterval(fetchSensorDataTimer.current);
        }
        };
    }, [fetchSensorData]);
    return (
        <div className='historicalData'>
            <h4>Historical Data</h4>
        <div>
          <span id="useMetric">
            Use Metric Units ({inMetric ? "ON" : "OFF"})
          </span>
          <Switch checked={inMetric} onChange={setInMetric} />
        </div>
            <div>
                <h2>Temperature</h2>
                <SensorChart sensorData={sensorData} dataKey="temperature" />
            </div>
            <div>
                <h2>Humidity</h2>
                <SensorChart sensorData={sensorData} dataKey="humidity" />
            </div>
            <div>
                <h2>Pressure</h2>
                <SensorChart sensorData={sensorData} dataKey="pressure" />
            </div>
            <div>
                <h2>Sound Level</h2>
                <SensorChart sensorData={sensorData} dataKey="sound_level" />
            </div>
        </div>
    );
})

HistoricalDataCharts.displayName = "HistoricalDataCharts";