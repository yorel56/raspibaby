import React from 'react';
import SSRProvider from 'react-bootstrap/SSRProvider';
import { Route, Switch } from 'react-router-dom';
import Home from './Home';
import './App.css';

const App = () => (
  <SSRProvider>
    <Switch>
      <Route exact={true} path="/" component={Home} />
    </Switch>
  </SSRProvider>
);

export default App;