# Python API Startup Guide

Welcome to the Python API startup guide! This guide will help you get started with setting up and running the Python API for your project.

## Prerequisites

Before you begin, make sure you have the following prerequisites installed on your system:

- Python 3.x
- Pip (Python package installer)

## Installation

To install the necessary dependencies for the Python API, follow these steps:

1. Open a terminal or command prompt.
2. Navigate to the project directory: `~/raspibaby/api/`.
3. Run the following command to install the required packages:

    ```
    ./rasp-setup.sh
    ```

## Configuration

Next, you need to configure the Python API. Follow these steps:

1. Create a `.env` file in the root api folder
2. Open the `config.py` file located in the project directory.
3. Modify the configuration variables according to your needs. Make sure to set the correct database connection details, API keys, and any other required settings

## Starting the API

To start the Python API, run the following command in your terminal or command prompt:

```
python3 app.py
```

Once the API is running, you can access it by navigating to `http://localhost:5000` in your web browser.

## Contributing

If you would like to contribute to the Python API, just open a pull request and have at it.
That's it! You have successfully set up and started the Python API for your project. Happy coding!
