# Step 1: Ensure Python3 and python3-venv are installed
sudo apt update
sudo apt install -y python3 python3-venv python3-pip
pip install -r requirements.txt
