import io
from threading import Lock
from picamera2 import Picamera2

class CameraModule:
    def __init__(self):
        self.camera = Picamera2()
        self.camera.configure(self.camera.create_still_configuration(main={"size": (640, 480)}))
        self.camera_lock = Lock()
        self.camera.start()

    def capture_video(self):
        with self.camera_lock:
            stream = io.BytesIO()
            self.camera.capture_file(stream, format = "jpeg")
            stream.seek(0)
            return stream.getvalue()

    def close(self):
        with self.camera_lock:
            self.camera.stop()

    def __del__(self):
        self.close()
