from apscheduler.schedulers.background import BackgroundScheduler
from orchestrator import Orchestrator

class Scheduler:
    def __init__(self, orchestrator, poll_interval, cleanup_interval):
        self.orchestrator = orchestrator
        self.poll_interval = poll_interval
        self.cleanup_interval = cleanup_interval
        self.scheduler = BackgroundScheduler()

    def start(self):
        self.scheduler.add_job(self.orchestrator.poll_sensors, 'interval', seconds=self.poll_interval)
        self.scheduler.add_job(self.orchestrator.cleanup_db, 'interval', seconds=self.cleanup_interval)
        self.scheduler.start()

    def stop(self):
        self.scheduler.shutdown()
