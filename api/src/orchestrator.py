class Orchestrator:
    def __init__(self, camera, temp_sensor, sound_sensor, motion_sensor, speaker, db_manager):
        self.camera = camera
        self.temp_sensor = temp_sensor
        self.sound_sensor = sound_sensor
        self.motion_sensor = motion_sensor
        self.speaker = speaker
        self.db_manager = db_manager

    def get_video_feed(self):
        return self.camera.capture_video()

    def play_audio(self, audio_stream):
        self.speaker.play_audio_stream(audio_stream)

    def get_sensor_data(self):
        temperature = self.temp_sensor.read_temperature()
        humidity = self.temp_sensor.read_humidity()
        sound_level = self.sound_sensor.detect_sound_level()
        motion_detected = self.motion_sensor.detect_motion()
        pressure = self.temp_sensor.read_pressure()  # Assuming temp_humidity_sensor has this method
        
        data = {
            "temperature": temperature,
            "humidity": humidity,
            "pressure": pressure,
            "sound_level": sound_level,
            "motion_detected": motion_detected
        }
        return data

    def poll_sensors(self):
        temperature = self.temp_sensor.read_temperature()
        humidity = self.temp_sensor.read_humidity()
        pressure = self.temp_sensor.read_pressure()  # Assuming temp_humidity_sensor has this method
        sound_level = self.sound_sensor.detect_sound_level()
        motion_detected = self.motion_sensor.detect_motion()

        with self.db_manager.get_connection() as conn:
            self.db_manager.insert_data(conn, temperature, humidity, pressure, sound_level, motion_detected)

    def cleanup_db(self):
        with self.db_manager.get_connection() as conn:
            self.db_manager.delete_all_data(conn)

    def get_historical_data(self, limit=None):
            with self.db_manager.get_connection() as conn:
                return self.db_manager.select_data(conn, limit)
