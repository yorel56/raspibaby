import sqlite3
from datetime import datetime

class SensorDataManager:
    def __init__(self, db_path='sensor_data.db'):
        self.db_path = db_path
        self.create_table()

    def get_connection(self):
        conn = sqlite3.connect(self.db_path)
        conn.row_factory = sqlite3.Row
        return conn

    def create_table(self):
        with self.get_connection() as conn:
            conn.execute('''CREATE TABLE IF NOT EXISTS sensor_data (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                temperature REAL,
                                humidity REAL,
                                pressure REAL,
                                sound_level REAL,
                                motion INTEGER,
                                timestamp INTEGER
                            )''')
            conn.commit()

    def insert_data(self, conn, temperature, humidity, pressure, sound_level, motion):
        timestamp = int(datetime.now().timestamp() * 1000)
        conn.execute('''INSERT INTO sensor_data (temperature, humidity, pressure, sound_level, motion, timestamp)
                        VALUES (?, ?, ?, ?, ?, ?)''', (temperature, humidity, pressure, sound_level, motion, timestamp))
        conn.commit()

    def select_data(self, conn, limit=None):
        query = 'SELECT * FROM sensor_data ORDER BY timestamp ASC'
        if limit:
            query += ' LIMIT ?'
        cursor = conn.execute(query, (limit,) if limit else ())
        data = cursor.fetchall()
        return [dict(row) for row in data]

    def delete_all_data(self, conn):
        conn.execute('DELETE FROM sensor_data')
        conn.commit()
