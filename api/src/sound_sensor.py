import spidev
import time

class SoundSensor:
    def __init__(self, spi_channel=0, spi_bus=0, adc_channel=0):
        """
        Initializes the SoundSensor with SPI channel and bus, and the ADC channel.
        
        :param spi_channel: SPI channel to use (usually 0 or 1)
        :param spi_bus: SPI bus to use (usually 0)
        :param adc_channel: ADC channel on the MCP3008 to read from (0-7)
        """
        self.spi_channel = spi_channel
        self.spi_bus = spi_bus
        self.adc_channel = adc_channel
        self.spi = spidev.SpiDev()
        self.spi.open(self.spi_bus, self.spi_channel)
        self.spi.max_speed_hz = 1350000  # Set the SPI clock speed

    def read_adc(self):
        """
        Reads the analog value from the specified ADC channel on the MCP3008.
        
        MCP3008 uses a 10-bit ADC, so the result will be in the range 0-1023.
        
        :return: The raw ADC value (0-1023)
        """
        # MCP3008 protocol to read from the specified ADC channel
        cmd = 0b11 << 6  # Start bit and single-ended mode bit
        cmd |= (self.adc_channel & 0x07) << 3  # Channel number shifted into the correct position
        # Send the command and read the response
        result = self.spi.xfer2([cmd, 0x0, 0x0])
        # The result is a 3-byte array. Combine the relevant bits to get the 10-bit result.
        # result[1] & 0x0F extracts the lower 4 bits of the second byte (result[1])
        # result[2] is the third byte which contains the lower 8 bits of the result
        adc_value = ((result[1] & 0x0F) << 8) | result[2]
        return adc_value

    def detect_sound_level(self):
        """
        Reads the raw analog value from the ADC and converts it to a voltage.
        
        Assumes a reference voltage of 3.3V for the ADC.
        
        :return: The voltage corresponding to the sound level
        """
        # Read the raw ADC value
        adc_value = self.read_adc()
        # Convert the raw ADC value to a voltage (assuming a 3.3V reference voltage)
        voltage = (adc_value * 3.3) / 1023.0
        return voltage

    def close(self):
        """
        Closes the SPI connection.
        """
        self.spi.close()

    def __del__(self):
        """
        Ensures the SPI connection is closed when the instance is deleted.
        """
        self.close()
