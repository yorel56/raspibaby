import RPi.GPIO as GPIO
from flask import Flask, Response, jsonify, request
from config import Config
from orchestrator import Orchestrator
from camera import CameraModule
from temp_humidity_sensor import TempHumiditySensor
from sound_sensor import SoundSensor
from motion_sensor import MotionSensor
from speaker import Speaker
from sensor_data_manager import SensorDataManager
from scheduler import Scheduler
from flask_cors import CORS

GPIO.setmode(GPIO.BCM)
app = Flask(__name__)
CORS(app)

# Initialize components with config values
camera = CameraModule()
temp_sensor = TempHumiditySensor(Config.I2C_PORT, Config.I2C_ADDRESS)
sound_sensor = SoundSensor(spi_channel=0, spi_bus=0, adc_channel=0)
motion_sensor = MotionSensor(Config.GPIO_MOTION)
speaker = Speaker()

# Initialize the sensor data manager
db_manager = SensorDataManager()

# Initialize the orchestrator with the components
orchestrator = Orchestrator(camera, temp_sensor, sound_sensor, motion_sensor, speaker, db_manager)

# Initialize and start the scheduler
scheduler = Scheduler(orchestrator, Config.POLL_INTERVAL_SECONDS, Config.CLEANUP_INTERVAL_SECONDS)
scheduler.start()

@app.route('/video_feed')
def video_feed():
    def generate():
        while True:
            frame = orchestrator.get_video_feed()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
    return Response(generate(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/play_audio', methods=['POST'])
def play_audio():
    audio_stream = request.files['audio']
    orchestrator.play_audio(audio_stream)
    return 'Audio played', 200

@app.route('/sensor_data')
def sensor_data():
    data = orchestrator.get_sensor_data()
    return jsonify(data)

@app.route('/historical_data')
def historical_data():
    limit = request.args.get('limit', default=None, type=int)
    data = orchestrator.get_historical_data(limit)
    return jsonify(data)

if __name__ == '__main__':
    app.run(host=Config.IP_ADDRESS, port=5000)
