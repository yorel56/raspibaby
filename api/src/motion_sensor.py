import RPi.GPIO as GPIO
import time

class MotionSensor:
    def __init__(self, gpio_pin):
        self.gpio_pin = gpio_pin
        GPIO.setup(self.gpio_pin, GPIO.IN)
        # Initialize the sensor hardware

    def detect_motion(self):
        return GPIO.input(self.gpio_pin) == GPIO.HIGH
