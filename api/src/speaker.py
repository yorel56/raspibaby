from pydub import AudioSegment
from pydub.playback import play

import time

class Speaker:
    def __init__(self):
        """
        Initializes the Speaker class with PyAudio setup.
        """

    def play_audio_stream(self, audio_stream):
        """
        Plays an audio stream through the default audio output.
        
        :param audio_stream: The audio stream to play (a file-like object)
        """
        sound = AudioSegment.from_file(audio_stream)
        play(sound)

    def close(self):
        """
        Closes the PyAudio instance.
        """

    def __del__(self):
        """
        Ensures the PyAudio instance is terminated when the instance is deleted.
        """
        self.close()
