import os
from dotenv import load_dotenv

load_dotenv()

class Config:
    GPIO_CAMERA = int(os.getenv('GPIO_CAMERA'))
    GPIO_SOUND = int(os.getenv('GPIO_SOUND'))
    GPIO_MOTION = int(os.getenv('GPIO_MOTION'))
    IP_ADDRESS = os.getenv('IP_ADDRESS')
    I2C_ADDRESS = os.getenv('I2C_ADDRESS')
    I2C_PORT = os.getenv('I2C_PORT')
    POLL_INTERVAL_SECONDS = int(os.getenv('POLL_INTERVAL_SECONDS', 60))  # Default to 60 seconds
    CLEANUP_INTERVAL_SECONDS = int(os.getenv('CLEANUP_INTERVAL_SECONDS', 86400))  # Default to 1 day
