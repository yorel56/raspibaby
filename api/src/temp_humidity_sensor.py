import smbus2
import bme280

class TempHumiditySensor:
    def __init__(self, i2c_port, i2c_address):
        self.i2c_port = 1
        self.i2c_address = 0x76
        self.bus = smbus2.SMBus(1)
        self.calibration_params = bme280.load_calibration_params(self.bus, self.i2c_address)

    def read_temperature(self):
        data = bme280.sample(self.bus, self.i2c_address, self.calibration_params)
        return data.temperature

    def read_humidity(self):
        data = bme280.sample(self.bus, self.i2c_address, self.calibration_params)
        return data.humidity

    def read_pressure(self):
        data = bme280.sample(self.bus, self.i2c_address, self.calibration_params)
        return data.pressure
